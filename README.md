# Step Decorators

Compile or run flows and steps locally. Steps run in isolated compute containers

First build an image with python and numpy as a dependency, then run the test flow via:
```
python -m app.my_flow
```

Example Flow with a few steps and a transformer

```python
from .flows import BaseGraph, step, transform_step, transformer
from .inputs import MLAsset, Transformation


class SequentialFlow(BaseFlow):

  # always needs a start
  @step(
    cls_name="Start",
    input_type=dict,
    output_type=Transformation)
  def start(self, input:dict):

    # defines what is the next step
    transform = Transformation.from_dict({
      "id": "transform-1",
      "user-type": "decision-tree",
    })

    self.next(self.my_compute, transform)



  @transform_step(cls_name="com.yada.project.TransformStep",
    image="test-flow:local.1",
    input_type=Transformation,
    output_type=Transformation)
  def my_compute(self, input):
      """
      @transform_step annotation represents a step doing computation,
      cls_name: class of step in target orchestration system
      image: docker image with all dependencies to run this step
      the step runs in isolated docker container
      input_type: declares input to this step, will be checked by compilation
      output_type: declares output of this step, will be checked by compilation
      """

      # using numpy but could as well use tensorflow or whatever
      import numpy as np
      fake_input = np.array([[1.0 for i in range(14)]])

      @transformer(function_name="scaling_transform") # annotates the computation for reporting
      def my_pipeline_transform():
          # this executes in compute engine, fancy stuff can be done via decorators
          fake_output = np.array([[1.0 for i in range(22)]])
          return fake_output

      my_pipeline_transform()



      logging.info(f"np array: {fake_input}")

      logging.info(f"running train step: " + json.dumps(input))

      transform = Transformation.from_dict({
        "id": "transfrom-branch", "user-type":"something"
      })

      self.next(self.end, transform)



  # always needs an end
  @step(cls_name="End",
    input_type=Transformation,
    output_type=None)
  def end(self, input):
    return


# now test
graph = SequentialFlow({"start": "ignition"})
graph.compile(target="json")

# run the graph,  
# but first build the test-flow image with numpy as dependency, check dockerfile
graph.run()

#compile input classes to java
asset = Transformation()
asset.compile(target="orch_asset")


```
