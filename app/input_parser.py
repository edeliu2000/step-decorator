import ast
import inspect


def get_asset_decorators(cls):
    target = cls
    decorators = {
      "cls_name": cls.__name__,
      "super": cls.__bases__[-1].__name__ if len(cls.__bases__) else None
     }

    def get_keyword_val(keywords, key):
        for k in keywords:
            if k.arg == key:
                if isinstance(k.value, ast.Name):
                    return k.value.id
                elif isinstance(k.value, ast.Str):
                    return k.value.s

        return None

    def visit_FunctionDef(node):
        for n in node.decorator_list:
            name = ''
            if isinstance(n, ast.Call):
                name = n.func.attr if isinstance(n.func, ast.Attribute) else n.func.id
            else:
                name = n.attr if isinstance(n, ast.Attribute) else n.id

            if name == 'MLParameter':
                decorators[node.name] = {"type": get_keyword_val(n.keywords, "type")}
                if get_keyword_val(n.keywords, "name"):
                    decorators[node.name]["f_name"] = get_keyword_val(n.keywords, "name")




    node_iter = ast.NodeVisitor()
    node_iter.visit_FunctionDef = visit_FunctionDef
    node_iter.visit(ast.parse(inspect.getsource(target)))
    return decorators
