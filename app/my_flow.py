
import json
import logging

from typing import Mapping, List, Tuple


from .flows import BaseGraph, step, transform_step, transformer
from .inputs import Transformation


class TestGraph(BaseGraph):
    """
    graph that does a sequence of steps, plus a branch
    """


    @step(cls_name="StartStep",
      input_type=dict,
      output_type=Transformation)
    def start(self, input:dict):
        # defines what is the next step
        transform = Transformation.from_dict({
          "id": "transform-1",
          "user-type": "decision-tree",
        })
        self.next(self.train, transform)



    @transform_step(
      cls_name="com.yada.project.TransformStep",
      input_type=Transformation,
      output_type=Transformation,
      image="test-flow:local.1")
    def train(self, input:Transformation):
        """
        @transform_step annotation represents a step doing computation,
        cls_name: class of step in target orchestration system
        image: docker image with all dependencies to run this step
        the step runs in isolated docker container
        """

        import numpy as np
        fake_input = np.array([[1.0 for i in range(14)]])

        @transformer(function_name="scaling_transform") # annotates the computation for reporting
        def my_pipeline_transform():
            # this executes in compute engine, fancy stuff can be done via decorators
            fake_output = np.array([[1.0 for i in range(22)]])
            return fake_output

        my_pipeline_transform()



        logging.info(f"np array: {fake_input}")

        logging.info(f"running train step: {input}")

        transform = Transformation.from_dict({
          "id": "transfrom-branch", "user-type":"something"
         })

        # defines a branch
        self.next(self.metrics_1, self.metrics_2, transform)



    @transform_step(
      cls_name="com.yoda.project.ComputeMetrics",
      input_type=Transformation,
      output_type=Transformation,
      image="test-flow:local.1")
    def metrics_1(self, input:Transformation):
        logging.info(f"running metric step-1: {input}")
        output = Transformation.from_dict(input.to_dict())

        self.next(self.join, join_input=output)


    @transform_step(
      cls_name="com.yada.project.ComputeMetrics",
      input_type=Transformation,
      output_type=Transformation,
      image="test-flow:local.1")
    def metrics_2(self, input:Transformation):
        logging.info(f"running metric step-2: {input}")
        output = Transformation.from_dict(input.to_dict())

        self.next(self.join, join_input=output)

    @step(cls_name="JoinStep",
      input_type=List[Transformation],
      output_type=Transformation)
    def join(self, join_input:List[Transformation] = None):
        print(f"running graph end: {join_input}")
        self.next(self.end, [])


    @step(cls_name="EndStep",
      input_type=Transformation,
      output_type=None)
    def end(self, input:Transformation):
        print(f"running graph end: {input}")
        return None







graph = TestGraph({"start": "ignition"})
graph.compile(target="json")

# run the graph,  but first build the test-flow image with numpy as dependency
#graph.run()
#asset = Transformation()
#asset.compile(target="orch_asset")
