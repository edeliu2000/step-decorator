import functools
import logging
import json
import os



from .input_parser import get_asset_decorators


def MLParameter(name=None, type=None):

    def decorator_param(func):
        @functools.wraps(func)
        def wrapper_param(*args, **kwargs):

            v = func(*args, **kwargs)
            return v

        return wrapper_param
    return decorator_param



class BaseAsset():

    def __init__(self):

        self._decorators = get_asset_decorators(self.__class__)


    def to_dict(self):
        meta = {}
        # all props
        for k, v in self.__dict__.items():
            if k != "_decorators":
                meta[k] = v

        return meta


    @classmethod
    def from_dict(cls, asset:dict):
        o = cls()
        for k, v in asset.items():
            setattr(o, k, v)

        return o





    def compile(self, target="json"):

        # template method for outputing an mlparameter in java
        def get_mlparam_method(method_name, method):
            method_type = "void"
            method_cls = "void"
            if method["type"] == "str":
                method_type = "String"
                method_cls = "String"
            elif method["type"] == "int":
                method_type = "Integer"
                method_cls = "Integer"
            elif method["type"] == "list":
                method_type = "List<>"
                method_cls = "ArrayList"

            f_name = method["f_name"] if "f_name" in method else None
            # camel case for field names
            camel = method_name.replace("get", "")[0].lower() + method_name.replace("get", "")[1:]


            return f"""

    @MLParameter(name = "{f_name or camel}")
    public {method_type} {method_name} {{
        return getParameterIfExists({method_cls}.class, "{f_name or camel}")
    }}
            """



        # method for outputing all mlparameter
        def get_mlparam_methods(methods):
            res = ""
            for m in methods:
                res = res + get_mlparam_method(m, self._decorators[m])

            return res



        if target == "json":
            # pure json representation
            print(self._decorators)
        elif target == "orch_asset":
            # orch representation
            print(
f"""
public class {self._decorators["cls_name"]} {("extends " + self._decorators["super"]) if "super" in self._decorators else ""} {{

   {get_mlparam_methods([ m for m in self._decorators if m not in ["cls_name", "super"]])}
}}
"""
            )

        else:
            #dunno anything else
            pass




class MLAsset(BaseAsset):

    @MLParameter(name="canonicalName", type=str)
    def getCanonicalName(self):
        return getattr(self, "canonicalName", None)

    @MLParameter(name="ref", type=str)
    def getReference(self):
        return getattr(self, "ref", None)


    def setCanonicalName(self, c):
        self.canonicalName = c


    def setReference(self, c):
        self.ref = c


class Transformation(MLAsset):

    @MLParameter(name="id", type=str)
    def getID(self):
        return getattr(self, "id", None)

    @MLParameter(name="user_type", type=str)
    def getType(self):
        return getattr(self, "user_type", None)

    @MLParameter(name="transform_func", type=str)
    def getTransformFunction(self):
        return getattr(self, "transform_func", None)


    def setID(self, c):
        self.id = c

    def setType(self, c):
        self.user_type = c

    def setTransformFunction(self, c):
        self.transform_func = c



class Dataset(MLAsset):

    @MLParameter(name="id", type=str)
    def getID(self):
        return getattr(self, "id", None)

    @MLParameter(name="user_type", type=str)
    def getType(self):
        return getattr(self, "user_type", None)

    @MLParameter(name="s3_uri", type=str)
    def getS3Uri(self):
        return getattr(self, "s3_uri", None)



    def setID(self, c):
        self.id = c

    def setType(self, c):
        self.user_type = c

    def setS3Uri(self, c):
        self.s3_uri = c



class MetricSet(MLAsset):

    @MLParameter(name="id", type=str)
    def getID(self):
        return getattr(self, "id", None)

    @MLParameter(name="user_type", type=str)
    def getType(self):
        return getattr(self, "user_type", None)


    def setID(self, c):
        self.id = c

    def setType(self, c):
        self.user_type = c
