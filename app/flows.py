import functools
import logging
import json
import os
import subprocess

from .flow_parser import FlowGraph
from .inputs import MLAsset

import sys

def transform_step(cls_name="ComputeStep", input_type=None, output_type=None, image=None):
    """
    decorator for steps that perform transforms/computation
    transform steps are always performed on isolated container/compute
    cls_name: class name for the step of the target orchestration system to compile
    (aws step functions, orch, airflow, etc.)
    """

    def decorator_step(func):
        @functools.wraps(func)
        def wrapper_step(*args, **kwargs):

            obj = args[0]

            # this section executes as part of flow process NOT in isolated compute
            if not obj.startFunc:
                print(f"calling transform step: {cls_name}")
                print(f"executing func step: {func.__name__}")

                last_arg = args[-1].to_dict() if isinstance(args[-1], MLAsset) else args[-1]

                # here the step function delegates to some execution engine
                # to perform isolated compute
                cmd = f"docker run --env flow_start_func={func.__name__} --env flow_start_func_input='{json.dumps(last_arg)}' --rm {image}"
                p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
                out, err = p.communicate()

                # this part handles computation results
                results = out.decode('utf-8')
                print(f"finished running transform step: {cls_name} - {results}")
                results = json.loads(results)

                node_name = func.__name__

                if obj.get_node(node_name).type == "split-and":
                    obj.add_branching(node_name, results)


                for result in results:

                    if obj.get_node(result["name"]).type == "join":
                        getattr(obj, result["name"])(join_input=result["input"])
                    else:
                        getattr(obj, result["name"])(result["input"])


            else: # this section executes on an isolated container as part of compute task


                # convert input to type declared in decorator from json
                if "join_input" in kwargs:
                    kwargs["join_input"] = [input_type.from_dict(a) if input_type else a for a in kwargs["join_input"]]
                    all_args = args
                else:
                    last_arg = input_type.from_dict(args[-1]) if input_type else args[-1]
                    all_args = []
                    for a in range(len(args) - 1):
                        all_args.append(args[a])
                    all_args.append(last_arg)

                # now execute the function
                v = func(*all_args, **kwargs)
                return v


        return wrapper_step
    return decorator_step


# step annotation
def step(cls_name="Step", input_type=None, output_type=None):
    def deco_step(func):
        @functools.wraps(func)
        def wrapper_step(*args, **kwargs):
            print(f"calling step: {cls_name}")

            obj = args[0]
            node_name = func.__name__

            if obj.get_node(node_name).type == "split-and":
                num_branches = len(obj.get_node(node_name).out_funcs)
                obj.add_branching(node_name, range(num_branches))


            # if its a join don't advance until all results come in
            if obj.get_node(node_name).type == "join":
                split_parent = obj.get_node(node_name).split_parents[-1]
                print(f"prior splits - {obj.get_node(node_name).split_parents}")
                num_res = obj._results[split_parent]["num_results"]
                obj.add_branching_event(split_parent, kwargs["join_input"])

                # return all inputs from branches
                if num_res == len(obj.get_branching_events(split_parent)):
                    kwargs["join_input"] = obj.get_branching_events(split_parent)
                    v = func(*args, **kwargs)
                    print(f"completed join step: {cls_name}")
                    return v
                else:
                    print(f"step event: {cls_name}")


            else:
                v = func(*args, **kwargs)
                print(f"completed step: {cls_name}")
                return v

        return wrapper_step
    return deco_step



# transformer annotation on compute functions for reporting start/completion
def transformer(function_name="transformer"):
    def deco_transformer(func):
        @functools.wraps(func)
        def wrapper_transformer(*args, **kwargs):
            logging.info(f"running transformer: {func.__name__ or function_name}")
            v = func(*args, **kwargs)
            logging.info(f"completed transformer: {func.__name__ or function_name}")
            return v

        return wrapper_transformer
    return deco_transformer




class BaseGraph():

    def __init__(self, input):
        self.graphInput = input
        self._steps = []
        self._results = {}

        self._graph = FlowGraph(self.__class__)

        #json representation
        self._graph_steps = [{
          "name": node.name,
          "type": node.type,
          "args": node.out_args,
          "step_flow_class": node.step_target_cls_name,
          "step_input_type": node.step_input_type,
          "step_output_type": node.step_output_type,
          "split_parents": node.split_parents} for node in self._graph]

        # start function to start graph execution from
        # relevant for running isolated steps in container
        self.startFunc = os.getenv('flow_start_func')
        # start function input to start graph execution with
        # relevant for running isolated steps in container
        self.startFuncInput = os.getenv('flow_start_func_input')



    def get_node(self, name):
        return self._graph[name]



    def add_branching(self, name, res):
        """
        adds a branching construct and expected branching results
        """
        self._results[name] = {
          "num_results":len(res),
          "results":[]
        }

    def add_branching_event(self, name, evt):
        """
        adds a result event when one branch completes
        """
        self._results[name]["results"].append(evt)


    def get_branching_events(self, name):
        return self._results[name]["results"]


    def run(self):

        step = None
        if self.startFunc and self.startFuncInput:

            # start flow from method passed in env variables
            if self.get_node(self.startFunc).type == "join":
                step = getattr(self, self.startFunc)(join_input=json.loads(self.startFuncInput))
            elif self.get_node(self.startFunc).type == "foreach":
                step = getattr(self, self.startFunc)(foreach=json.loads(self.startFuncInput))
            else: # maybe condition here too??
                step = getattr(self, self.startFunc)(json.loads(self.startFuncInput))

        else:
            # or start flow from its proper start method
            step = self.start(self.graphInput)

        # now iterate flow steps if in non docker mode
        if not self.startFunc:
            while step:
                print("step execution -----")
                step = next(step)




    def compile(self, target="json"):
        """
        compiles into some fancy graph execution engine with high availability and scale
        """

        def validate_input_output():
            prev_type = None
            prev_node = None
            for n in self._graph_steps:
                name = "name"

                #check join type
                if n["type"] == "join":
                    if isinstance(n["step_input_type"], tuple) and prev_type != n["step_input_type"][1] :
                        print(f"Error - mismatched types: {prev_node[name]}:output: {prev_type} - {n[name]}:input: {n['step_input_type']} - joins inputs are lists of elements whose type should match that of the preceeding node output")

                    elif isinstance(n["step_input_type"], str) and prev_type != n["step_input_type"] :
                        print(f"Error - mismatched types: {prev_node[name]}:output: {prev_type} - {n[name]}:input: {n['step_input_type']} - joins inputs are lists of elements whose type should match that of the preceeding node output")

                #check prev node output to current node input type
                elif n[name] != "start" and prev_type != n["step_input_type"]:
                    print(f"Error - mismatched types: {prev_node[name]}:output: {prev_type} - {n[name]}:input: {n['step_input_type']}")

                prev_type = n["step_output_type"]
                prev_node = n


        validate_input_output()

        if target == "json":
            print(self._graph_steps)
        else:
            #dunno anything else
            pass



    def start(self, input):
        pass



    def next(self, *args, **kwargs):
        # when running inside a isolated container as a step
        if self.startFunc:
            # communicate back to flow process what is the next function and its input
            funcs = []

            if "join_input" in kwargs:
                out = kwargs["join_input"].to_dict() if isinstance(kwargs["join_input"], MLAsset) else kwargs["join_input"]
                funcs.append({
                  "name": args[-1].__name__,
                  "input": out
                })

            for i in range(len(args) - 1):
                out = args[-1].to_dict() if isinstance(args[-1], MLAsset) else args[-1]

                funcs.append({
                  "name": args[i].__name__,
                  "input": out
                })

            print(json.dumps(funcs))

        else:
            # handle branching, and wait for results of all dependencies
            print(f"kwargs - {kwargs}")
            if "join_input" in kwargs:
                args[-1](join_input=kwargs["join_input"])
            else:
                for i in range(len(args) - 1):
                    args[i](args[-1])
